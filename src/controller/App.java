package controller;

public class App {
     private String appName;
     private String port;
     private String fileAddress;

    public App(String fileAddress,String appName, String port) {
        this.appName = appName;
        this.port = port;
        this.fileAddress = fileAddress;
    }

    public String getAppName() {
        return appName;
    }

    public String getPort() {
        return port;
    }

    public String getFileAddress() {
        return fileAddress;
    }

}
