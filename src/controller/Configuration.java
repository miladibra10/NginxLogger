package controller;
import java.io.*;
import java.util.ArrayList;

public class Configuration {
    private static final int cycleDuration = 1; ///In Milliseconds and must be int!
    private static final String mysqlDriver = "com.mysql.jdbc.Driver";
    private static final String mysqlUser = "root";
    private static final String mysqlPass = "est946p1376104";
    private static final String connectionStr = "jdbc:mysql://localhost/";
    private static final String adminUser = "admin";
    private static final String adminPass = "est946p1376104";
    private static final String adminMail = "miladibra10@gmail.com";
    private static final String dataBaseName = "profiler";
    private static File file = new File("/Resources/Apps");
    private static final String appsFile = System.getProperty("user.dir")+file.getPath();

    public static ArrayList<App> getApps() throws IOException, ClassNotFoundException {
        Class config = Class.forName("controller.Configuration");
        ClassLoader classLoader = config.getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("Resources/Apps");
        ArrayList<App> apps = new ArrayList<>();
        BufferedReader appBuffer = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = appBuffer.readLine()) != null) {
            String[] params = line.split(" ");
            App app = new App(params[0],params[1],params[2]);
            apps.add(app);
        }
        appBuffer.close();
        return apps;
    }
    public static String getDataBaseName() {
        return dataBaseName;
    }

    public static String getAdminUser() {
        return adminUser;
    }

    public static String getAdminPass() {
        return adminPass;
    }

    public static String getAdminMail() {
        return adminMail;
    }

    public static int getCycleDuration() {
        return cycleDuration;
    }

    public static String getMysqlDriver() {
        return mysqlDriver;
    }

    public static String getConnectionString() {
        return connectionStr+getDataBaseName();
    }

    public static String getMysqlUser() {
        return mysqlUser;
    }

    public static String getMysqlPass() {
        return mysqlPass;
    }
}
