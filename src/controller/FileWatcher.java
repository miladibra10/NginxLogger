package controller;

import model.*;

import java.io.*;
import java.util.TimerTask;

public class FileWatcher extends TimerTask {
    private long timeStamp;
    private File file;
    private LogManager logManager;
    private String port;
    private String appName;

    public FileWatcher(File file) {
        this.file = file;
        this.timeStamp = file.lastModified();
        this.logManager = new LogManager();
    }

    public FileWatcher(File file,String appName ,String port) {
        this.file = file;
        this.timeStamp = file.lastModified();
        this.logManager = new LogManager();
        this.port = port;
        this.appName = appName;
    }

    @Override
    public void run() {
        long timeStamp = file.lastModified();

        if (this.timeStamp != timeStamp) {
            this.timeStamp = timeStamp;
            onChange();
        }
    }

    public void onChange() {
        try {
            FileReader logReader = new FileReader(this.file);
            BufferedReader logBuffer = new BufferedReader(logReader);
            String line;
            while ((line = logBuffer.readLine()) != null) {
                ///Parse will be Done Here
                System.out.println(line);
                Log tempLog = new Log(line,appName,port);
                ///Adding to database
                logManager.insertLog(tempLog);

            }
            logBuffer.close();
            PrintWriter cleaner = new PrintWriter(this.file);
            cleaner.write("");
            cleaner.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("File Changed");
    }
}
