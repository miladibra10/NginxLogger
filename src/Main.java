import controller.App;
import controller.Configuration;
import controller.FileWatcher;

import java.io.File;
import java.util.*;

public class Main {
    public static void main(String[] args) {

//        Scanner scanner = new Scanner(System.in);
//        String command = scanner.next();
//        if(command.equals("install"))
//        {
//            Install.install();
//        }
//        else
//        {
            try {
                ArrayList<App> apps = Configuration.getApps();
                for (App app : apps) {
                    TimerTask task = new FileWatcher(new File(app.getFileAddress()), app.getAppName(), app.getPort());
                    Timer timer = new Timer();
                    timer.schedule(task, new Date(), Configuration.getCycleDuration());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//        }
    }
}
