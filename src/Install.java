import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import controller.Configuration;
import controller.EncoderUtil;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Install {
    public static void install() {
        try {
            Class.forName(Configuration.getMysqlDriver());
            Connection connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/", Configuration.getMysqlUser(), Configuration.getMysqlPass());
            createDataBase(connection);
            createAppTable(connection);
            createIPTable(connection);
            createProtocolTable(connection);
            createTypesTable(connection);
            createURLTable(connection);
            createUserTable(connection);
            addAdminUser(connection);
            createRequestTable(connection);
            connection.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void createDataBase(Connection connection) throws SQLException {
        Statement statement = (Statement) connection.createStatement();
        statement.execute("CREATE DATABASE "+Configuration.getDataBaseName());
    }

    private static void createIPTable(Connection connection) throws SQLException {

        Statement statement = (Statement) connection.createStatement();
        statement.execute("use "+Configuration.getDataBaseName());
        statement.execute("CREATE TABLE IPs (IP_ID int NOT NULL AUTO_INCREMENT,IPAddress varchar(15) NOT NULL UNIQUE ,PRIMARY KEY (IP_ID))");
    }

    private static void createTypesTable(Connection connection) throws SQLException {
        Statement statement = (Statement) connection.createStatement();
        statement.execute("use "+Configuration.getDataBaseName());
        statement.execute("CREATE TABLE RequestTypes (Type_ID int NOT NULL AUTO_INCREMENT,RequestType varchar(20) NOT NULL UNIQUE ,PRIMARY KEY (Type_ID))");
    }

    private static void createProtocolTable(Connection connection) throws SQLException {
        Statement statement = (Statement) connection.createStatement();
        statement.execute("use "+Configuration.getDataBaseName());
        statement.execute("CREATE TABLE Protocols (Protocol_ID int NOT NULL AUTO_INCREMENT,Protocol varchar(20) NOT NULL UNIQUE ,PRIMARY KEY (Protocol_ID))");
    }

    private static void createURLTable(Connection connection) throws SQLException {
        Statement statement = (Statement) connection.createStatement();
        statement.execute("use "+Configuration.getDataBaseName());
        statement.execute("CREATE TABLE URLs (URL_ID int NOT NULL AUTO_INCREMENT,URL varchar(100) NOT NULL UNIQUE ,PRIMARY KEY (URL_ID))");
    }

    private static void createAppTable(Connection connection) throws SQLException {
        Statement statement = (Statement) connection.createStatement();
        statement.execute("use "+Configuration.getDataBaseName());
        statement.execute("CREATE TABLE Apps (App_ID int NOT NULL AUTO_INCREMENT,AppName varchar(100) NOT NULL UNIQUE ,Port varchar(5),PRIMARY KEY (App_ID))");
    }

    private static void createUserTable(Connection connection) throws SQLException {
        Statement statement = (Statement) connection.createStatement();
        statement.execute("use "+Configuration.getDataBaseName());
        statement.execute("CREATE TABLE Users (User_ID int NOT NULL AUTO_INCREMENT,Username varchar(20) NOT NULL,Password varchar(128) NOT NULL ,Email varchar(60), PRIMARY KEY (User_ID))");
    }

    private static void createRequestTable(Connection connection) throws SQLException {
        Statement statement = (Statement) connection.createStatement();
        statement.execute("use "+Configuration.getDataBaseName());
        statement.execute("CREATE TABLE Requests (" +
                "Request_ID int NOT NULL AUTO_INCREMENT," +
                "IP_ID int NOT NULL," +
                "RequestBody VARCHAR(255)," +
                "Type_ID int NOT NULL," +
                "URL_ID int NOT NULL," +
                "Protocol_ID int NOT NULL," +
                "App_ID int NOT NULL," +
                "Status int NOT NULL," +
                "BytesSent int NOT NULL," +
                "RequestTime DOUBLE(7,3) NOT NULL," +
                "PRIMARY KEY (Request_ID)," +
                "FOREIGN KEY (IP_ID) REFERENCES IPs(IP_ID)," +
                "FOREIGN KEY (Type_ID) REFERENCES RequestTypes(Type_ID)," +
                "FOREIGN KEY (URL_ID) REFERENCES URLs(URL_ID)," +
                "FOREIGN KEY (Protocol_ID) REFERENCES Protocols(Protocol_ID)," +
                "FOREIGN KEY (App_ID) REFERENCES Apps(App_ID)" +
                ")");
    }

    private static void addAdminUser(Connection connection) throws Exception {
        Statement statement = (Statement) connection.createStatement();
        statement.execute("use "+Configuration.getDataBaseName());
        String userName = Configuration.getAdminUser();
        String hashPass = EncoderUtil.getMD5(Configuration.getAdminPass());
        String email = Configuration.getAdminMail();
        PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO Users (Username,Password,Email) VALUES (?,?,?)");
        preparedStatement.setString(1,userName);
        preparedStatement.setString(2,hashPass);
        preparedStatement.setString(3,email);
        preparedStatement.execute();
    }

}
