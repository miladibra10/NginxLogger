package model;

import controller.*;
import com.mysql.jdbc.*;

import java.sql.DriverManager;
import java.sql.SQLException;

public class LogDA {
    private Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public LogDA() {
//        try {
//            Class.forName(Configuration.getMysqlDriver());
//            connection = (Connection) DriverManager.getConnection(Configuration.getConnectionString(), Configuration.getMysqlUser(), Configuration.getMysqlPass());
//            connection.setAutoCommit(false);
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }

    public void connectToDB()
    {
        try {
            Class.forName(Configuration.getMysqlDriver());
            connection = (Connection) DriverManager.getConnection(Configuration.getConnectionString(), Configuration.getMysqlUser(), Configuration.getMysqlPass());
            connection.setAutoCommit(false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertLog(Log log) {
        this.connectToDB();
        int ipID, typeID, URLID, protocolID,appID;
        ipID = insertIP(log.getRemoteAddress());
        typeID = insertType(log.getRequest().getRequestType());
        URLID = insertURL(log.getRequest().getRequestAddress());
        protocolID = insertProtocol(log.getRequest().getRequestProtocol());
        appID = insertApp(log.getAppName(),log.getPort());
        if (ipID == -1 || typeID == -1 || URLID == -1 || protocolID == -1 || appID==-1) {
            System.out.println("ERROR in adding request");
            return;
        } else {
            try {
                preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO Requests (IP_ID,RequestBody,Type_ID,URL_ID,Protocol_ID,Status,BytesSent,RequestTime,App_ID) VALUES (?,?,?,?,?,?,?,?,?)");
                preparedStatement.setInt(1, ipID);
                preparedStatement.setString(2, log.getRequestBody());
                preparedStatement.setInt(3, typeID);
                preparedStatement.setInt(4, URLID);
                preparedStatement.setInt(5, protocolID);
                preparedStatement.setInt(6, log.getStatus());
                preparedStatement.setInt(7, log.getBytesSent());
                preparedStatement.setDouble(8, log.getRequestTime());
                preparedStatement.setDouble(9, appID);
                preparedStatement.execute();
                connection.commit();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    private int insertIP(String ip) {
        try {
            preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO IPs (IPAddress) VALUES (?)");
            preparedStatement.setString(1, ip);
            preparedStatement.execute();
        } catch (SQLException e) {
//            e.printStackTrace();
            System.out.println("Duplicate Data in IP");
        } finally {
            try {
                preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT IP_ID FROM IPs WHERE IPAddress=?");
                preparedStatement.setString(1, ip);
                resultSet = (ResultSet) preparedStatement.executeQuery();
                resultSet.next();
                return resultSet.getInt("IP_ID");
            } catch (SQLException e) {
                System.out.println("IP_ID Not Fount");
                return -1;
            }
        }
    }

    private int insertType(String type) {
        try {
            preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO RequestTypes (RequestType) VALUES (?)");
            preparedStatement.setString(1, type);
            preparedStatement.execute();
        } catch (SQLException e) {
            System.out.println("Duplicate Data in Request Type");
        } finally {
            try {
                preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT Type_ID FROM RequestTypes WHERE RequestType=?");
                preparedStatement.setString(1, type);
                resultSet = (ResultSet) preparedStatement.executeQuery();
                resultSet.next();
                return resultSet.getInt("Type_ID");
            } catch (SQLException e) {
                System.out.println("Type_ID Not Fount");
                return -1;
            }
        }
    }

    private int insertURL(String URL) {
        try {
            preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO URLs (URL) VALUES (?)");
            preparedStatement.setString(1, URL);
            preparedStatement.execute();
        } catch (SQLException e) {
            System.out.println("Duplicate Data in URL");
        } finally {
            try {
                preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT URL_ID FROM URLs WHERE URL=?");
                preparedStatement.setString(1, URL);
                resultSet = (ResultSet) preparedStatement.executeQuery();
                resultSet.next();
                return resultSet.getInt("URL_ID");
            } catch (SQLException e) {
                System.out.println("URL_ID Not Fount");
                return -1;
            }
        }
    }

    private int insertProtocol(String protocol) {
        try {
            preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO Protocols (Protocol) VALUES (?)");
            preparedStatement.setString(1, protocol);
            preparedStatement.execute();
        } catch (SQLException e) {
            System.out.println("Duplicate Data in Protocol");
        } finally {
            try {
                preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT Protocol_ID FROM Protocols WHERE Protocol=?");
                preparedStatement.setString(1, protocol);
                resultSet = (ResultSet) preparedStatement.executeQuery();
                resultSet.next();
                return resultSet.getInt("Protocol_ID");
            } catch (SQLException e) {
                System.out.println("Protocol_ID Not Fount");
                return -1;
            }
        }
    }

    private int insertApp(String appName , String port)
    {
        try {
            preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO Apps (AppName,Port) VALUES (?,?)");
            preparedStatement.setString(1, appName);
            preparedStatement.setString(2,port);
            preparedStatement.execute();
        } catch (SQLException e) {
            System.out.println("Duplicate Data in App");
        } finally {
            try {
                preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT App_ID FROM Apps WHERE AppName=? AND Port=?");
                preparedStatement.setString(1, appName);
                preparedStatement.setString(2, port);
                resultSet = (ResultSet) preparedStatement.executeQuery();
                resultSet.next();
                return resultSet.getInt("App_ID");
            } catch (SQLException e) {
                System.out.println("App_ID Not Fount");
                return -1;
            }
        }
    }

}
