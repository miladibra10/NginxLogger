package model;

public class LogManager {
    LogDA logDa;

    public LogManager() {
        logDa = new LogDA();
    }

    public void insertLog(Log log) {
        logDa.insertLog(log);
    }
}
