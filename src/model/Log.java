package model;

public class Log {

    private String remoteAddress;
    private String requestBody;
    private Request request;
    private int status;
    private int bytesSent;
    private float requestTime;
    private String appName;
    private String port;

    public Log(String log ,String appName , String port) {
        request = new Request();
        String[] params = log.split(",");
        ///Adding remote address
        this.setRemoteAddress(params[0]);
        ///Adding request body
        this.setRequestBody(params[1]);
        ///Adding request
        String[] reqParams = params[2].split(" ");
        this.request.setRequestType(reqParams[0]);
        this.request.setRequestAddress(reqParams[1]);
        this.request.setRequestProtocol(reqParams[2]);
        ///Adding status
        this.setStatus(Integer.parseInt(params[3]));
        ///Adding bytes sent
        this.setBytesSent(Integer.parseInt(params[4]));
        ///Adding request time
        this.setRequestTime(Float.parseFloat(params[5]));
        this.setPort(port);
        this.setAppName(appName);
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getBytesSent() {
        return bytesSent;
    }

    public void setBytesSent(int bytesSent) {
        this.bytesSent = bytesSent;
    }

    public float getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(float requestTime) {
        this.requestTime = requestTime;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
